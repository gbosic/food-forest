R: edibility rating, h: height, r: radius, ft: flowering time, sr: seed ripening time, gr: growth rate

1 no shade emergent plants:  Carya illinoinensis ("Pecan") R:3 h:40m r:10m , ft:4 - 5, sr:10, gr:M
there are 1 emergent plants
1 full shade canopy plants:  Hedera helix ("Ivy") R:0 h:15m r:2.5m , ft:10 - 11, sr:5 - 6, gr:M
5 semi shade canopy plants:  Pinus sylvestris ("Scot's Pine") R:2 h:25m r:5m , ft:5, sr:3 - 6, gr:F Fraxinus americana ("White Ash") R:1 h:25m r:7.5m , ft:4 - 5, sr:9, gr:F Acer platanoides ("Norway Maple") R:2 h:21m r:7.5m , ft:4 - 5, sr:9 - 10, gr:F Sorbus aucuparia ("Mountain Ash") R:2 h:15m r:3.5m , ft:5 - 6, sr:8 - 9, gr:M Thuja occidentalis ("American Arbor-Vitae") R:2 h:15m r:2.5m , ft:4 - 5, sr:9 - 10, gr:S
2 no shade canopy plants:  Picea omorika ("Serbian Spruce") R:2 h:25m r:2.5m , ft:5, sr:10 - 11, gr:F Juniperus virginiana ("Pencil Cedar") R:2 h:20m r:4m , ft:4 - 5, sr:10, gr:S
there are 8 canopy plants
11 semi shade tree plants:  Carpinus caroliniana ("American Hornbeam") R:2 h:12m r:5m , ft:4 - 5, sr:11, gr:S Crataegus phaenopyrum ("Washington Thorn") R:2 h:10m r:5m , ft:7, sr:10, gr: Crataegus douglasii ("Black Hawthorn") R:4 h:9m r:4.5m , ft:5, sr:9, gr: Juniperus communis ("Juniper") R:4 h:9m r:2m , ft:5 - 6, sr:10, gr:S Corylus heterophylla ("Siberian Filbert") R:5 h:7m r:3.5m , ft:4 - 5, sr:9 - 10, gr: Crataegus arnoldiana ("") R:5 h:7m r:3.5m , ft:5, sr:8 - 9, gr: Crataegus rotundifolia ("") R:3 h:6m r:2m , ft:6, sr:9, gr: Cornus mas ("Cornelian Cherry") R:4 h:5m r:2.5m , ft:2 - 3, sr:9, gr:M Viburnum opulus ("Guelder Rose") R:3 h:5m r:2.5m , ft:6 - 7, sr:9 - 10, gr:M Viburnum lantana ("Wayfaring Tree") R:2 h:5m r:2m , ft:5 - 6, sr:7 - 9, gr:M Buxus sempervirens ("Box") R:1 h:5m r:2.5m , ft:4 - 5, sr:9, gr:S
3 no shade tree plants:  Elaeagnus angustifolia ("Oleaster") R:4 h:7m r:3.5m n, ft:6, sr:9 - 10, gr:M Caragana arborescens ("Siberian Pea Tree") R:5 h:6m r:2m n, ft:5, sr:9, gr:F Syringa vulgaris ("Lilac") R:1 h:6m r:3m , ft:5, sr:8, gr:M
2 nitrogen fixing tree plants:  Elaeagnus angustifolia ("Oleaster") R:4 h:7m r:3.5m n, ft:6, sr:9 - 10, gr:M Caragana arborescens ("Siberian Pea Tree") R:5 h:6m r:2m n, ft:5, sr:9, gr:F
there are 14 tree plants
1 full shade shrub plants:  Forsythia x intermedia ("") R:0 h:2.5m r:1.25m , ft:3 - 4, sr:, gr:
5 semi shade shrub plants:  Corylus americana ("American Hazel") R:3 h:4m r:2m , ft:4 - 5, sr:9 - 10, gr: Philadelphus coronarius ("Mock Orange") R:0 h:4m r:2m , ft:6, sr:, gr:F Corylus cornuta ("Beaked Hazel") R:5 h:3m r:1.5m , ft:4 - 5, sr:9 - 10, gr: Ligustrum vulgare ("Privet") R:0 h:3m r:1.5m , ft:6 - 7, sr:9 - 10, gr:M Asparagus officinalis ("Asparagus") R:4 h:1.5m r:0.375m , ft:8, sr:9 - 10, gr:
2 no shade shrub plants:  Juniperus sabina ("Savine") R:0 h:4m r:2m , ft:4, sr:10, gr:S Elaeagnus commutata ("Silverberry") R:3 h:3m r:0.75m n, ft:5, sr:7 - 9, gr:M
1 nitrogen fixing shrub plants:  Elaeagnus commutata ("Silverberry") R:3 h:3m r:0.75m n, ft:5, sr:7 - 9, gr:M
there are 8 shrub plants
1 semi shade herb plants:  Achillea millefolium ("Yarrow") R:4 h:0.6m r:0.3m , ft:6 - 8, sr:7 - 9, gr:
2 no shade herb plants:  Nepeta cataria ("Catmint") R:3 h:1m r:0.3m , ft:7 - 11, sr:9 - 10, gr: Juniperus horizontalis ("Creeping Juniper") R:2 h:1m r:1.5m , ft:, sr:10, gr:
there are 3 herb plants
1 full shade ground cover plants:  Pachysandra terminalis ("Japanese Spurge") R:2 h:0.2m r:0.25m , ft:4 - 5, sr:9 - 10, gr:M
3 semi shade ground cover plants:  Coriandrum sativum ("Coriander") R:3 h:0.45m r:0.125m , ft:6 - 7, sr:8 - 9, gr: Festuca ovina ("Sheep's Fescue") R:1 h:0.25m r:0.125m , ft:5 - 6, sr:, gr: Chamaemelum nobile ("Camomile") R:4 h:0.15m r:0.15m , ft:6 - 7, sr:7 - 10, gr:
1 no shade ground cover plants:  Trifolium repens ("White Clover") R:3 h:0.1m r:0.5m n, ft:6 - 9, sr:7 - 10, gr:M
1 nitrogen fixing ground cover plants:  Trifolium repens ("White Clover") R:3 h:0.1m r:0.5m n, ft:6 - 9, sr:7 - 10, gr:M
there are 5 ground cover plants

